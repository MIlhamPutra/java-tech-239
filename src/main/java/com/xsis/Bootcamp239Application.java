package com.xsis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bootcamp239Application {

	public static void main(String[] args) {
		SpringApplication.run(Bootcamp239Application.class, args);
	}

}
